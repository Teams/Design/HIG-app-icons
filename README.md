This repository contains essential assets to start creating application
icons that follow the GNOME Human Interface Guidelines:

https://developer.gnome.org/hig/guidelines/app-icons.html
